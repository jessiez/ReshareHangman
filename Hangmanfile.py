import random


wordlist = open('list.txt').readlines() #Import wordlist
wordlist = [i.replace('\n','') for i in wordlist] #Get rid of all the space.
secret_word = random.choice(wordlist).upper() #get a random word from list.
gameover = False #to end loop when necessary

hangman_graph = ['''
    ____________
    |
    |
    |
    |
    |
    |
    ------------''','''
    ____________
    |     |
    |     0
    |
    |
    |
    |
    ------------''','''
    ____________
    |     |
    |     0
    |    \|
    |
    |
    |
    ------------''','''
    ____________
    |     |
    |     0
    |    \|/
    |
    |
    |
    ------------''','''
    ____________
    |     |
    |     0
    |    \|/
    |    /
    |
    |
    ------------''','''
    ____________
    |     |
    |     0
    |    \|/
    |    / \
    |
    |
    ------------''']  #The hangman graph board

correct = [] #Letter that user guesses right.
wrong = [] #Letter that user guesses wrong.
score = 0 #User score

#print('Testing: %s' % secret_word) #Test
print('Welcome to the game Hangman.') #Instruction
print('You have 5 chances to guess the word before the little man is hanged!')


def draw():
    print(hangman_graph[len(wrong)]) #Corresponding hangman draw.
    print('Your word has %s letters.' % len(secret_word)) #Indicate how many letter the word has.
    for i in secret_word:
        if i in correct:
            print(i, end=' ') #Seperate each letter with a space.
        else:
            print ('_', end=' ') #Use line to represent each letter.
    print ('\n\nWrong Letters')
    for i in wrong:
        print(i,end=' ')
    print('\n====================')

def input_Guess():
    while True:
        guess = input('Guess a letter\n: ').upper() #Get user input.
        if guess in correct or guess in wrong: #If user is already in correct or incorrect list.
            print('You have already guessed that letter. Try again.')
        elif guess.isnumeric(): #make sure the input is not numbers
            print('Please enter only letters, not numbers. Try again.')
        elif len(guess) > 1: #check if the input has more than 1 letter
            print("Please enter only one letter at a time. Try again.")
        elif len(guess) == 0: #make sure to input something
            print('Please enter your selections.')
        else:
            break
    if guess in secret_word: #If userguess is one letter in secret_word
        correct.append(guess)
    else: #If userguess is not a letter in secret_word
        wrong.append(guess)

def check_win():
    if len(wrong) > 5: #User has five guesses, if there are over 5 letters in incorrect list, user loses the game.
        return 'loss' #User loses
    for i in secret_word:
        if i not in correct:
            return 'no win'
    return 'win' #User wins

def playagain(): #The function returns True if the player wants to play again.
    print('Do you want to play again? (y/n)')
    return input().lower().startswith('y')

while True:
    draw() #Call draw function
    input_Guess() #Call user_guess function
    win_condition = check_win() #Set win_condition to check_win
    if win_condition == 'loss': #If user gets it wrong.
        print('Game Over. The word was %s.' % secret_word)
        print ('You score is %s' % score)
        gameover = True
    elif win_condition == 'win': #If user gets it right.
        print ('You win. The word was %s' % secret_word)
        score += 1
        print ('You score is %s' % score)
        gameover = True
    if gameover:
        if playagain(): #If user wants to play again, reset correct and incorrect list, set gameIsDone False, and generate a random word.
            correct = []
            wrong = []
            gameover = False
            secret_word = random.choice(wordlist).upper()
        else: #If user doesn't want to play again.
            break #Exit.
